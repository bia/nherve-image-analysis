package plugins.nherve.toolbox.imageanalysis.modules;

import plugins.nherve.toolbox.image.feature.region.IcyPixel;
import plugins.nherve.toolbox.image.feature.signature.VectorSignature;

public class PixelSignatureData {
	public IcyPixel pix;
	public VectorSignature sig;
}
